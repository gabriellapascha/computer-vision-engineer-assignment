from __future__ import print_function
import io
import os
from enum import Enum
from statistics import mode, median
import numpy as np
from PIL import Image, ImageDraw
from google.cloud import vision


class FeatureType(Enum):
    PAGE = 1
    BLOCK = 2
    PARA = 3
    WORD = 4
    SYMBOL = 5


def draw_boxes_new(image, bounds, color):
    """Draw a border around the image using xmin, ymin,x_max, y_max"""
    draw = ImageDraw.Draw(image)
    # x_min = bounding_box.vertices[0].x
    # y_min = bounding_box.vertices[0].y
    # x_max = bounding_box.vertices[2].x
    # y_max = bounding_box.vertices[2].y
    # [x_min, y_min, x_max, y_max]
    for bound in bounds:
        draw.polygon([
            bound[0], bound[1],
            bound[2], bound[1],
            bound[2], bound[3],
            bound[0], bound[3]], None, color)
    return image


def get_document_google(image_file):
    """Use google vision API for  text detection"""
    # Authentificaiton
    os.environ[
        "GOOGLE_APPLICATION_CREDENTIALS"] = PATH_TO_CREDENTIALS

    client = vision.ImageAnnotatorClient()

    with io.open(image_file, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)

    # response = client.document_text_detection(image=image)
    response = client.text_detection(image=image)
    document = response.full_text_annotation
    return document

def reset_box(type):
    if type == 'average':
        return np.inf, 0, 0, 0
    else:
        return np.inf, np.inf, 0, 0


def get_line_bbox_width(paragraph):
    lines_list = []
    lines_bbox_list = []
    line_ave_sym_width_list = []
    line = ''
    type = 'average'
    x_min, y_min, x_max, y_max = reset_box(type)
    symbol_width = 0
    num_symbols = 0
    for word in paragraph.words:
        for symbol in word.symbols:
            line += symbol.text
            num_symbols += 1
            symbol_width += symbol.bounding_box.vertices[2].x - symbol.bounding_box.vertices[0].x
            if symbol.bounding_box.vertices[0].x < x_min:
                x_min = symbol.bounding_box.vertices[0].x

            if symbol.bounding_box.vertices[2].x > x_max:
                x_max = symbol.bounding_box.vertices[2].x

            if type == 'average':
                y_max += symbol.bounding_box.vertices[2].y
                y_min += symbol.bounding_box.vertices[0].y
            else:
                if symbol.bounding_box.vertices[0].y < y_min:
                    y_min = symbol.bounding_box.vertices[0].y
                if symbol.bounding_box.vertices[2].y > y_max:
                    y_max = symbol.bounding_box.vertices[2].y

            if str(symbol.property.detected_break) == 'type_: SPACE\n':
                line += ' '
            if str(symbol.property.detected_break) == 'type_: EOL_SURE_SPACE\n':
                lines_list.append(line)
                line = ''
                line_ave_sym_width_list.append(int(symbol_width / num_symbols))
                symbol_width = 0
                if type == 'average':
                    lines_bbox_list.append([x_min, y_min / num_symbols, x_max, y_max / num_symbols])
                else:
                    lines_bbox_list.append([x_min, y_min, x_max, y_max])
                num_symbols = 0

                x_min, y_min, x_max, y_max = reset_box(type)
            if str(symbol.property.detected_break) == 'type_: LINE_BREAK\n':  # end of paragraph
                lines_list.append(line)
                line_ave_sym_width_list.append(int(symbol_width / num_symbols))
                if type == 'average':
                    lines_bbox_list.append([x_min, y_min / num_symbols, x_max, y_max / num_symbols])
                else:
                    lines_bbox_list.append([x_min, y_min, x_max, y_max])
    return lines_list, lines_bbox_list, line_ave_sym_width_list


def height_compare(lines_bbox_list):
    heights = []
    for i in lines_bbox_list:
        height = np.int(i[3] - i[1])
        heights.append(height)
    return heights


def get_mode(list):
    """Find the most common value"""
    all_values = []
    for para in list:
        for lin in para:
            if not np.isnan(lin):
                all_values.append(lin)
    try:
        moode = mode(all_values)
    except:
        # print('No mode found will take median')
        moode = median(all_values)
    return moode


def get_diff_mode(para_list, list_mode):
    """get the difference from the mode"""
    diff = []
    for para in para_list:
        diff_temp = []
        for lin in para:
            diff_temp.append(lin - list_mode)
        diff.append(diff_temp)
    return diff


def get_line_diff(bboxs):
    """Find the distance between lines with in paragraphs, 'NAN' if only 1 line in paragraph"""
    line_diff = []
    for para in bboxs:
        if len(para) > 1:
            diff = []
            for i in range(0, len(para) - 1):
                diff.append(para[i + 1][3] - para[i][1])
            line_diff.append(diff)
        else:
            line_diff.append([np.nan])
    return line_diff


def get_uppers(llines):
    full_upper = []
    first_uppers = []
    for para in llines:
        lin_full_uppers = []
        first_upper = []
        for lin in para:
            if lin[0].isupper():
                first_upper.append(1)
            if not lin[0].isupper():
                first_upper.append(0)
            if lin.isupper():
                lin_full_uppers.append(1)
            if lin.islower():
                lin_full_uppers.append(0)
            if not lin.islower() and not lin.isupper():
                capitals = 0
                for sym in lin:
                    if sym.isupper():
                        capitals += 1
                lin_full_uppers.append((capitals / len(lin.split())) - 0.01)  ## number of capitals by number of words
                # print(str(capitals) + ' and ' + str(len(lin)))
                # lin_full_uppers.append(0.5)
        full_upper.append(lin_full_uppers)
        first_uppers.append(first_upper)
    return full_upper, first_uppers


def get_line_info(document):
    heights = []
    bboxs = []
    llines = []
    widths = []

    for page in document.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                lines, bbox, ave_width = get_line_bbox_width(paragraph)
                bboxs.append(bbox)
                llines.append(lines)
                height = height_compare(bbox)
                heights.append(height)
                widths.append(ave_width)

    line_space = get_line_diff(bboxs)
    upperss, fist_upper = get_uppers(llines)
    return widths, heights, bboxs, llines, upperss, line_space, fist_upper


def predict_heading_boxes_own(widths, heights, bboxs, llines):
    width_mode = get_mode(widths)
    width_diff = get_diff_mode(widths, width_mode)

    height_mode = get_mode(heights)
    height_diff = get_diff_mode(heights, height_mode)

    line_space = get_line_diff(bboxs)
    line_diff_mode = get_mode(line_space)
    lin_space_diff = get_diff_mode(line_space, line_diff_mode)
    upperss, first_uppers = get_uppers(llines)

    para_counts = []
    for i in range(0, len(llines)):
        t = len(width_diff[i])
        counts = []
        if t > 1:
            for j in range(0, t):
                count = get_counts(width_diff, height_diff, lin_space_diff, upperss, i, j, t, first_uppers)
                counts.append(count)
        else:
            count = get_counts(width_diff, height_diff, lin_space_diff, upperss, i, 0, t, first_uppers)
            counts.append(count)
        para_counts.append(counts)

    bounds = []
    headers = []
    for i in range(0, len(llines)):
        t = len(llines[i])
        if t > 1:
            for j in range(0, t):
                if para_counts[i][j] > 1:
                    # print(llines[i][j])
                    headers.append(llines[i][j])
                    bounds.append(bboxs[i][j])
        else:
            if para_counts[i][0] > 1:
                # print(llines[i][0])
                headers.append(llines[i][0])
                bounds.append(bboxs[i][0])

    return bounds, headers

def get_counts(width_diff, height_diff, lin_space_diff, upperss, i, j, t, first_uppers):
    width = width_diff[i][j]
    height = height_diff[i][j]
    first_upper = first_uppers[i][j]

    if j < t - 1:
        space = lin_space_diff[i][j]
    else:
        space = 0

    capital = upperss[i][j]
    count = 0
    if first_upper == 1:
        count += 1
    if height > 10:
        count += 1
    if height < -10:
        count -= 1
    if width > 1:
        count += 1
    if height > 5 and width > 1:
        count += 1

    if space > 2 and space < 10:
        count += 1
    if capital > 0.5:
        count += 1
    return count


def main()
    """
    PATH_TO_CREDENTIALS on Line 40 must be added (google clould credentials)

    path to image to find headings. 

    can draw image with heading bounding box by setting draw_image to True.
    """
    # path = '/Users/gabriellapascha/PycharmProjects/computer-vision-engineer-assignment/test_images/img_002.png'
    path = '/Users/gabriellapascha/PycharmProjects/computer-vision-engineer-assignment/TEST3.png'


    g_document = get_document_google(path)
    w, h, b, l, u, ls, fu = get_line_info(g_document)
    header_bounds, headers = predict_heading_boxes_own(w, h, b, l)

    for heading in range(0, len(headers)):
        print("Heading " + str(heading + 1) + ': ' + str(headers[heading]))

    draw_image = False
    if draw_image is True:
        image = Image.open(path)
        draw_boxes_new(image, header_bounds, 'red')
        image.show()



